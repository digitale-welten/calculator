#!/usr/bin/env python3
# -*- coding: utf-8 -*-

v = True
while v:
    # Variables
    error = False
    operator = False
    result = 0
    x = input(">> ")
    y = [""]
    
    # Quit
    if x == "":
        v = False
    else:
        
        # Split
        for item in x:
            if (item in [i for i in range(10)]):
                y[len(y) - 1] += item
                operator = False
            elif (item in ["+", "-", "/", "*"]):
                if operator:
                    error = True
                
                y.append(item)
                y.append("")
                operator = True
            elif str(item == " "):
                pass
            else:
                error = True
        
        # Calcualte
        if error:
            print("Error")
        else:
            result = int(y[0])
            for z in range(2, len(y), 2):
                if y[z - 1] == "-":
                    result -= float(y[z])
                elif y[z - 1] == "*":
                    result *= float(y[z])
                elif y[z - 1] == "/":
                    result /= float(y[z])
                else:            
                    result += float(y[z])
        
            print(result)

