#!/usr/bin/env python3
# -*- coding: utf-8 -*-
"""
Created on Fri Jan 17 12:57:13 2020

@author: anthy
"""

def quadratischeFunktion(a, b, c, x=4):
    return a * a * x + b * x + c

quadratischeFunktion(1, 2, 3)


arr = ["a", "b", "c"]

#for element in arr:
#    print(element)

for index in range(len(arr)):
    print(index)
    element = arr[index]
    print(element)

index = 0
while index < len(arr):
    print(index)
    element = arr[index]
    print(element)
    index += 1


"a" += "b" # fügt einem string einen string an
str(number) # konvertiert / wandelt nummer in string um
