# Calculator

## Loops

### `for`-Loop

> Für jedes Element in einer Menge, tu das

Iteration über die Elemente von einem Array:

```python
for element in array:
	# do stuff with element..
```

Iteration über alle Indizes von einem Array:

```python
for index in range(len(array)):
	# do stuff with index..
	
	# e.g.:
	element = array[index]
```

### `while`-Loop

> So lange wie die Bedingung Wahr ist, tu das

```python
bedingung = True

while bedingung:
	# do stuff..
	
	# break out of loop
	bedingung = False
```

```python
zahl = 0

while zahl < 10:
	zahl = zahl + 1
	print(zahl)
```

So kann auch über die Indizes von einem Array iteriert werden:

```python
eine_liste = ["a", "b", "c", "d", "e"]
index = 0

while index < len(eine_liste):
	# do stuff with index..
	
	# e.g.:
	element = eine_liste[index]
	
	# "index" um Eins inkrementieren, um zum nächsten Index zu gehen
	index += 1
```

## Weitere Bemerkungen

### Listen

Listen sind veränderbare Mengen von Elementen. Jedes Element hat dabei einen einzigartigen Index, der die Position des Elements in der Liste angibt. Das erste Element in einer Liste hat den Index 0, und die Länge einer Liste ist die Anzahl der Element in dieser (somit kann der Index maximal gleich der Länge der Liste - 1 sein, da ja bei 0 angefangen wird zu zählen, und nicht bei 1).

#### Neue Elemente in Listen fügen

Der Datentyp list() hat, ähnlich wie z.B. str(), "lokale" Funktionen, sogenannte Methoden. Hat man also eine Variable vom Datentyp list(), so kann man z.B. die Methode `append` aufrufen um der Liste ein neues Element anzufügen.

```python
eine_liste = []
eine_liste.append("abc")
print(eine_liste)			# >> ["abc"]
eine_liste.append(123)
print(eine_list)			# >> ["abc", 123]
```

#### Ein Element aus einer Liste via dessen Index holen

Jedes Element in einer Liste kann über dessen Index in ihr abgefragt werden.

```python
eine_liste[0]	# >> "abc"
eine_liste[1]	# >> 123
eine_liste[2]	# Da die Liste "eine_liste" kein drittes Element besitzt, wird der folgende Fehler ausgeworfen: >> IndexError: list index out of range
```

### len()

Einige Datentypen, wie z.B. str() und list() haben eine von ihrem Wert abhängige Eigenschaft "Länge". Bei str() ist dies die Anzahl der Zeichen, bei list() die Anzahl der Elemente.

```python
len("abc")		# = 3
len([0, 1, 2])	# = 3
```

### Variablen-Re-deklarierung

Variablen können erneut deklariert werden. So kann etwa eine mathematische Operation in der selben Variable gespeichert werden, die auch in der Gleichung verwendet werden, deren Wert sie annimmt.

```python
ergebnis = 0				# = 0
ergebnis = ergebnis + 3		# = 3
ergebnis = 3 / ergebnis		# = 1
```

### `if Element in Array`

Das `in`-Statement kann, abgesehen bei `for`-Loops ebenfalls bei `if-elif`-Statements verwendet werden. Dadurch kann z.B. getestet werden, ob ein Element in einer Liste existiert, oder ein Zeichen in einem String.

```python
s = "abc"
print("a" in s)		# >> True
print("ab" in s)	# >> True
print("d" in s)		# >> False
print("ac" in s)	# >> False
```

```python
a = ["a", "b", "c"]
print("a" in a)		# >> True
print("ab" in a)	# >> False
print("d" in a)		# >> False
print("ac" in a)	# >> False
```

## Beispiele

### Einen `while`-Loop durch Eingabe vom Benutzer beenden

```python
loop_aktiv = True

while loop_aktiv:
	weiter_abfrage = input("Weiter? ")
	if weiter_abfrage == "nein":
		loop_aktiv = False
	
	# weiterer code...
	pass
```

