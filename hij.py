#!/usr/bin/env python3
# -*- coding: utf-8 -*-
"""
Created on Fri Jan 10 12:23:57 2020

@author: anthy
"""

def numberInput():
    number = None
    
    # Dieser while-loop endet erst, wenn eine benutzbare Nummer eingegeben wurde
    while number == None:
        try:
            number = float(input("Number >> "))
        except:
            print("Invalid number!")
    
    return number

operators = ["+", "-", "*", "/"]
def operatorInput():
    operator = None
    
    # Dieser while-loop endet erst, wenn ein benutzbarer Operator eingegeben wurde
    while operator == None:
        temp = input("Operator >> ")
        if temp in operators:
            operator = temp
        else:
            print("Invalid operator!")
    
    return operator

yes_es = ["y", "1", "yes", "ja"]
no_es = ["n", "0", "no", "nein"]
def checkYesNo(string, default=False):
    if string.lower() in yes_es:
        return True
    elif string.lower() in no_es:
        return False

    return default



# alle eingaben abfragen

number_inputs = []
operator_inputs = []

more_inputs = True

while more_inputs:
    if len(number_inputs) < 2:
        number_inputs.append(numberInput())
    
    operator_inputs.append(operatorInput())
    
    number_inputs.append(numberInput())
    
    more_inputs = checkYesNo(input("Do you want to continue? >> "))

print(number_inputs)
print(operator_inputs)


# punkt-vor-strich verarbeiten

equation = str(number_inputs[0])

index_tracked = 0

for index in range(len(operator_inputs)):
    operator = operator_inputs[index - index_tracked]
    number = number_inputs[index + 1 - index_tracked]

    equation += operator + str(number)

    if operator in ["*", "/"]:
        temp_result = number_inputs[index - index_tracked]

        if operator == "*":
            temp_result = temp_result * number
        elif operator == "/":
            temp_result = temp_result / number
        
        # Operator und eine Nummer entfernen, weil wir sie nicht mehr brauchen
        del operator_inputs[index - index_tracked]
        del number_inputs[index + 1 - index_tracked]

        # Nummer updaten, weil wir sie ja gerade schon eingerechnet haben
        number_inputs[index - index_tracked] = temp_result

        index_tracked += 1

print(number_inputs)
print(operator_inputs)


# eingaben verarbeiten (rechnen)

result = number_inputs[0]
equation_short = str(number_inputs[0])

for index in range(len(operator_inputs)):
    operator = operator_inputs[index]
    number = number_inputs[index + 1]
    
    if operator == "+":
        result = result + number
    elif operator == "-":
        result = result - number
    elif operator == "*":
        result = result * number
    elif operator == "/":
        result = result / number
    
    equation_short += operator + str(number)
    
print(equation, " = ", equation_short, " = ", result)
