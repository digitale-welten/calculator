# -*- coding: utf-8 -*-

"""
Ein paar nützliche Funktionsdefinitionen
"""

# ---
# Im Folgenden werden Funktionen definiert, bei denen einige Argumente bereits Standardwerte besitzen
# Erkennbar sind diese, weil das Argument in der Funktionsdefinition direkt ein Wert zugewiesen wird
# Bsp.:
# def FunktionsName(argument_1, argument_2=Standardwert):
#     pass
# Wird ein Argument mit Standardwert beim Funktionsaufruf nicht angegeben, so wird der Standardwert verwendet
# ---



# Eine Funktion, die Ja-oder-Nein fragt, und ansonsten einen Standardwert zurückgibt
def checkYesNoInput(string, default=False):
    
    # Folgend wird die Funktion "lower" auf der Variable "string" aufgerufen, welche alle Groß-geschriebenen Zeichen im String zu klein-geschriebenen umwandelt
    # Diese Funktion existiert bei allen Variablen vom Datentyp String
    
    # Überprüft, ob die Variable "string" im Grunde entweder "Ja" oder "Nein" ist
    
    if string.lower() in ["y", "yes", "1", "true", "j", "ja"]:
        return True
    elif string.lower() in ["n", "no", "0", "false", "nein"]:
        return False
    
    # Ansonsten wird der Standardwert zurückgegeben
    
    return default

# Eine Funktion, die die Eingabe von Nummern ermöglicht (und nur diesen!), und den Wert entweder als float() oder int() zurückgibt
def numberInput(n_type=0):
    number = None
    
    # Dieser while-loop endet erst, wenn eine benutzbare Nummer eingegeben wurde
    while number == None:
        if n_type == 0:
            try:
                number = float(input("Number (float) >> "))
            except:
                print("Invalid float!")
        
        elif n_type == 1:
            try:
                number = int(input("Number (integer) >> "))
            except:
                print("Invalid integer!")
    
    return number

# Alle Operatoren, die benutzbar sind
valid_operators = ["+", "-", "*", "/"]

# Eine Funktion, die die Eingabe von den Operatoren ermöglicht (und nur diesen!)
def operatorInput():
    operator = None
    
    # Dieser while-loop endet erst, wenn ein benutzbarer Operator eingegeben wurde
    while operator == None:
        operator_input = input("Operator [+-*/] >> ")
        
        # Überprüfung, ob die Eingabe auch wirklich ein benutzbarer Operator ist
        if operator_input in valid_operators:
            operator = operator_input
        else:
            print("Invalid operator!")
    
    return operator



"""
Als nächstes werden die Eingaben vom Benutzer geholt
Dafür werden die zuvor definierten Funktionen numberInput, operatorInput und checkYesNoInput genutzt
"""

# Zwei Listen, in denen wir die mit den zu rechnenden Nummern und Operatoren speichern
number_inputs = []
operator_inputs = []

# Eine Variable, die wir benutzen um den Abfrage-Loop zu beenden
more_inputs = True

# So lange der Nutzer weitere Eingaben machen möchte..
while more_inputs:
    # Beim ersten Durchlauf wird zuerst eine Nummer abgefragt
    if len(number_inputs) < 2:
        number_input_1 = numberInput()
        number_inputs.append(number_input_1)
    
    # Abfrage des Operators
    operator_input = operatorInput()
    operator_inputs.append(operator_input)
    
    # Abfrage der Nummer
    number_input_2 = numberInput()
    number_inputs.append(number_input_2)
    
    # Abfrage, ob noch ein weiterer Durchlauf für Nummer- und Operatoreingabe durchgeführt werden soll
    if len(number_inputs) >= 2:
        more_inputs = checkYesNoInput(input("Do you want to input additional data? [yN] "))

print(number_inputs)
print(operator_inputs)



"""
Nun werden alle Nummern und Operatoren aufeinander angewendet
"""

# Das Ergebnis startet mit der ersten eingegeben Nummer
result = number_inputs[0]

for index in range(len(operator_inputs)):
    # "holt" den Operator und die nächste Nummer
    operator = operator_inputs[index]
    number = number_inputs[index + 1] # + 1, weil wir eine Nummer mehr haben, als wir Operatoren haben, und die Nummer bei index=0 ist beim ersten Durchlauf bereits in result
    
    # Als nächstes wird je nach Operator die Nummer auf das derzeitige Ergebnis angewendet
    if operator == "+":
        result = result + number
    elif operator == "-":
        result = result - number
    elif operator == "*":
        result = result * number
    elif operator == "/":
        result = result / number

print("Result: ", result)
